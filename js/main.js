function validate() {
    let valid = [];
    valid.push(valField("firstname"));
    valid.push(valField("lastname"));
    valid.push(valField("email"));
    valid.push(valField("passwd"));
    for (const v of valid) {
        if (!v) {
            return false;
        }
    }
    return true;
}

function valField(id) {
    const field = document.getElementById(id);
    if (field.value === '') {
        field.classList.add("invalid");
        return false;
    }
    return true;
}
function valEmail(id) {
    const field = document.getElementById(id);
    const isValid = isEmailValid(field);
    if (isValid) {
        field.classList.add("invalid");
        return false;
    }
    return true;
}
function fieldKeyUp(field) {    
    if (field.value === '') {
        field.classList.add("invalid");
    } else {
        field.classList.remove("invalid");
    }
}
function emailKeyUp(field) {
    if (isEmailValid(field)) {
        field.classList.add("invalid");
    } else {
        field.classList.remove("invalid");
    }
}
function isEmailValid(field) {
    var emailReg = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
    return (field.value === '' || !emailReg.test(field.value));
}